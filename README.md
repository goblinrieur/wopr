# Play with chatGPT ans text-to speech at once 

_currently not working - API key expired and not regenerated_

-- Sat Apr 29 12:45:34 PM CEST 2023 -- WOPR__v0.0.1 -- dev startup

```
__        _____  ____  ____                 ___   ___   _ 
\ \      / / _ \|  _ \|  _ \        __   __/ _ \ / _ \ / |
 \ \ /\ / / | | | |_) | |_) |       \ \ / / | | | | | || |
  \ V  V /| |_| |  __/|  _ <         \ V /| |_| | |_| || |
   \_/\_/  \___/|_|   |_| \_\____ ____\_/  \___(_)___(_)_|
                           |_____|_____|                  
```

![pic](./wopr.jpg)

## You will need a chatGPT registered key 

You will get it from Official Open-AI's website [here](https://platform.openai.com/signup)

From profile menu search "View API Keys"

![pic](./pic1.png)

This opens a API keys main page 

![pic](./pic2.png)

You will use the "+ Create new secret key" each for any of your usages"

In 99% use cases, you need only one API key, just like for this FMP/WEP _ "Few Minutes project" _ or _ "Week-End Project" _ whatever you call it in your country.

```
cd
mkdir commandline-chatgpt
cd commandline-chatgpt
python3 -m venv chatgpt_cli
source commandline-chatgpt/chatgpt_cli/bin/activate
cd commandline-chatgpt/
echo "sk-gjklDeDVEuVC[.....]" > chatgptkey # use your own key !!!! 
export OPENAI_API_KEY="sk-gjklDeDVEuVC[.....]" # use your own key !!!!
pip3 install shell-gpt
sgpt "how to write a gnuforth word that calculates °C to °k convertion" > toto ; cat toto ; cat toto |  espeak -mb/mb-fr -s150 
```

# Time for a first try then 

Just use a temporary file as a buffer, _in example below it is called **toto**_. This is only needed of course if you wanna get both display and voice.

I choose to use both as far as voice sometimes pronounce strangely code or special characters & so on. 

```
(chatgpt_cli) 148-2404-453-~/commandline-chatgpt/chatGPT+speech $ sgpt "how to write a gnuforth word that calculates °C to °k convertion" > toto ; cat toto ; cat toto |  espeak -mb/mb-fr -s150 
To write a gnuforth word that calculates °C to °K conversion, you can use the following formula:

°C + 273.15 = °K

Here's an example gnuforth word that implements this formula:

: c-to-k ( c -- k ) 273.15 + ;

This word takes a temperature value in °C as input and returns the corresponding value in °K. To use it, simply call the word with the temperature value on the stack, like this:

20 c-to-k

This will return the value 293.15, which is the equivalent temperature in °K.
(chatgpt_cli) 0-2405-454-~/commandline-chatgpt/chatGPT+speech $
```

# Keep in mind it is cheap but not free.

See example here of intense hour work

![bill](./bills.png)

# Now test is all OK & it is time to script that all in one 

![play example](./play.png)

From now many improvements will be needed but is a quite good starting point
