#! /usr/bin/env bash
# goblinrieur@gmail.com

cmdchatpath="commandline-chatgpt"
chatgptpath="chatgpt_cli"
# create a memory buffer
> /dev/shm/chatgptbuffer

function install_debian()
{
	# Quick & dirty installed might be a temporary one
	echo "-- try an automatic installation on debian-like system --"
	currentpath=$(pwd)
	cd ${HOME}
	echo "-- You migth need to ente you sudo password --"
	sudo apt-get update
	sudo apt-get install -y python3 python3-pip espeak ansifilter espeak-data speech-dispatcher-espeak
	echo "-- workdir will be named ${cmdchatpath} --"
	mkdir ${cmdchatpath}
	cd ${cmdchatpath}
	python3 -m venv ${chatgptpath}
	source ${cmdchatpath}/${chatgptpath}/bin/activate
	echo "-- Please what is your OpenAI's account chatGPT APIkey ? --"
	read keyname
	export OPENAI_API_KEY=${keyname}
	echo 'export OPENAI_API_KEY="${keyname}"' > ./.realsecretkey
	pip3 install shell-gpt
	echo "Greeting Professor Falken." > toto ; cat toto ; cat toto |  espeak  -s150
	echo "-- install finished --"
	exit 0
}

function exiting()
{
	echo -e "\033[0m\n\n"
	echo "-- This is the end. Bye --" > /dev/shm/chatgptbuffer
	cat /dev/shm/chatgptbuffer # displays it
	cat /dev/shm/chatgptbuffer |  espeak  -s150 &# read it as audio
	echo -e "\n\n"
	exit 0
}

function run ()
{
	# Quick & dirty version to start developpement from 
	trap exiting INT
	clear
	echo -e "\033[41mCtrl+c to end/exit\033[0m"
	echo -e "\033[36m---"
	echo "Greetings Professor Falken!" > /dev/shm/chatgptbuffer 
	cat /dev/shm/chatgptbuffer & # displays it 
       	cat /dev/shm/chatgptbuffer |  espeak  -s150  # read it as audio
	source ./.realsecretkey
	while true; 
	do
		echo
		echo -e "\033[32mYour demand ?	\033[0m"
		read demand
		${HOME}/${cmdchatpath}/${chatgptpath}/bin/sgpt "${demand}" > /dev/shm/chatgptbuffer 
		echo -e "\033[36m"
		cat /dev/shm/chatgptbuffer & # displays it 
		cat /dev/shm/chatgptbuffer |  espeak  -s150 # read it as audio
	done
	echo
}

# add here condition test to run installation function 

run
exit 0 
		
